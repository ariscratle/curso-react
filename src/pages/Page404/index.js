import React from 'react'

import './page404.css'

const Page404 = () => {
    return (
        <div className="page404">
            <h1>Página não encontrada!</h1>
        </div>
    )
}

export default Page404