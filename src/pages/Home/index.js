import React, { Component, Fragment } from 'react';
import Cabecalho from '../../components/Cabecalho'
import NavMenu from '../../components/NavMenu'
import Dashboard from '../../components/Dashboard'
import Widget from '../../components/Widget'
import TrendsArea from '../../components/TrendsArea'
import Tweet from '../../containers/TweetContainer'
import Modal from '../../components/Modal'
import PropTypes from 'prop-types'
import * as TweetsActions from '../../actions/TweetsActions'

import './home.css'

class App extends Component {
    constructor() {
        super()
        this.state = {
            novoTweet: '',
            tweets: [],
            tweetAtivo: {}
        }
    }

    adicionaTweet = (event) => {
        event.preventDefault()

        if (this.state.novoTweet) {
            this.context.store.dispatch(TweetsActions.adicionaTweet(this.state.novoTweet))

            this.setState({
                novoTweet: ''
            })
        }
    }

    static contextTypes = {
        store: PropTypes.object
    }

    componentDidMount() {
        window.store = this.context.store
        
        this.context.store.subscribe(() => {
            this.setState({
                tweetAtivo: this.context.store.getState().tweets.tweetAtivo,
                tweets: this.context.store.getState().tweets.tweets
            })
        })

        this.context.store.dispatch(TweetsActions.carregaTweets())
    }

    enterKey = (event) => {
        if (event.keyCode === 13) {
            this.adicionaTweet(event);
        }
    }

    abreModalHandler = (idDoTweetQueVaiNoModal) => {
        this.context.store.dispatch({ type: 'ABRE_MODAL', idDoTweetQueVaiNoModal: idDoTweetQueVaiNoModal })
    }

    fechaModal = (evento) => {
        const elementoAtivo = evento.target
        const isModal = elementoAtivo.classList.contains('modal')

        if (isModal) {
            this.context.store.dispatch({ type: 'FECHA_MODAL' })
        }
    }

    render() {
        return (
        <Fragment>
            <Cabecalho>
                <NavMenu usuario="@omariosouto" />
            </Cabecalho>
            <div className="container">
                <Dashboard>
                    <Widget>
                        <form className="novoTweet" onSubmit={ this.adicionaTweet }>
                            <div className="novoTweet__editorArea">
                                <span className="novoTweet__status">
                                    { this.state.novoTweet.length }/140
                                </span>
                                <textarea
                                    onChange={ (event) => { this.setState({ novoTweet: event.target.value }) } }
                                    onKeyDown={ this.enterKey }
                                    value={ this.state.novoTweet }
                                    className="novoTweet__editor" 
                                    placeholder="O que está acontecendo?"></textarea>
                            </div>
                            <button type="submit" 
                                    disabled={ this.state.novoTweet.length > 140 }
                                    className="novoTweet__envia">Tweetar</button>
                        </form>
                    </Widget>
                    <Widget>
                        <TrendsArea />
                    </Widget>
                </Dashboard>
                <Dashboard posicao="centro">
                    <Widget>
                        <div className="tweetsArea">
                            {
                                this.state.tweets.length > 0 
                                ? 
                                this.state.tweets.map((tweetAtual, indice) => {
                                    return <Tweet 
                                            key={ tweetAtual._id } 
                                            id={ tweetAtual._id } 
                                            texto={ tweetAtual.conteudo }
                                            likeado={ tweetAtual.likeado }
                                            removivel={ tweetAtual.removivel }
                                            totalLikes={ tweetAtual.totalLikes }
                                            removeHandler={ () => { this.removeOTweet(tweetAtual._id) } }
                                            abreModalHandler={ () => { this.abreModalHandler(tweetAtual._id) } }
                                            usuario={ tweetAtual.usuario } />
                                }) 
                                :
                                <div className="carregando">Carregando tweets...</div>
                            }
                        </div>
                    </Widget>
                </Dashboard>
            </div>

            <Modal isAberto={ Boolean(this.state.tweetAtivo._id) } fechaModal={ this.fechaModal }>
                {
                    Boolean(this.state.tweetAtivo._id) &&
                    <Widget>
                    <Tweet 
                        id={ this.state.tweetAtivo._id } 
                        texto={ this.state.tweetAtivo.conteudo }
                        likeado={ this.state.tweetAtivo.likeado }
                        removivel={ this.state.tweetAtivo.removivel }
                        totalLikes={ this.state.tweetAtivo.totalLikes }
                        removeHandler={ () => { this.removeOTweet(this.state.tweetAtivo._id) } }
                        abreModalHandler={ () => { this.abreModalHandler(this.state.tweetAtivo._id) } }
                        usuario={ this.state.tweetAtivo.usuario } />
                    </Widget>
                }
            </Modal>

            {
                this.context.store.getState().notificacao &&
                <div className="notificacaoMsg" onAnimationEnd={() => {
                    this.context.store.dispatch({ type: 'REMOVE_NOTIFICACAO' })
                }}>
                    { this.context.store.getState().notificacao }
                </div>
            }
        </Fragment>
        );
    }
}

export default App;
