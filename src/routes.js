import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import Home from './pages/Home';
import LoginPage from './pages/LoginPage'
import Page404 from './pages/Page404'

class PrivaRoute extends Component {
    render() {
        if (localStorage.getItem("TOKEN")) {
            const ComponentQueVaiNaTela = this.props.component
            return (
                <Route component={ComponentQueVaiNaTela} />
            )
        } else {
            return (
                <Redirect to="/login" />
            )
        }
    }
}

class LogoutPage extends Component {
    render() {
        localStorage.removeItem("TOKEN")

        return <Redirect to="/login" />
    }
}

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                <PrivaRoute path="/" exact component={ Home } />
                <Route path="/login" component={ LoginPage } />
                <Route path="/logout" component={ LogoutPage } />
                <Route component={ Page404 } />
            </Switch>
        )
    }
}
